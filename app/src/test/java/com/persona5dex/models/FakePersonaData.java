package com.persona5dex.models;

/**
 * Created by Rechee on 7/8/2017.
 */

public class FakePersonaData {
    public static String fakePersonaFileContents() {
        return "[\n" +
                "\t{\n" +
                "\t\t\"name\": \"Abaddon\",\n" +
                "\t\t\"arcana\": \"Judgement\",\n" +
                "\t\t\"level\": 74,\n" +
                "\t\t\"stats\": [51, 38, 58, 43, 39],\n" +
                "\t\t\"elems\": [\"ab\", \"ab\", \"-\", \"-\", \"-\", \"-\", \"wk\", \"wk\", \"-\", \"ab\"],\n" +
                "\t\t\"skills\": {\n" +
                "\t\t\t\"Absorb Phys\": 79,\n" +
                "\t\t\t\"Deathbound\": 0,\n" +
                "\t\t\t\"Gigantomachia\": 80,\n" +
                "\t\t\t\"Makarakarn\": 0,\n" +
                "\t\t\t\"Spirit Drain\": 0,\n" +
                "\t\t\t\"Survival Trick\": 77\n" +
                "\t\t}\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"name\": \"Agathion\",\n" +
                "\t\t\"arcana\": \"Chariot\",\n" +
                "\t\t\"level\": 3,\n" +
                "\t\t\"stats\": [3, 4, 5, 7, 3],\n" +
                "\t\t\"elems\": [\"-\", \"rs\", \"-\", \"-\", \"rs\", \"wk\", \"-\", \"-\", \"-\", \"-\"],\n" +
                "\t\t\"skills\": {\n" +
                "\t\t\t\"Baisudi\": 0,\n" +
                "\t\t\t\"Dia\": 0,\n" +
                "\t\t\t\"Dodge Elec\": 8,\n" +
                "\t\t\t\"Lunge\": 4,\n" +
                "\t\t\t\"Rakukaja\": 6,\n" +
                "\t\t\t\"Zio\": 0\n" +
                "\t\t},\n" +
                "\t\t\"personality\": \"Timid\"\n" +
                "\t},\n" +
                "\t{\n" +
                "\t\t\"name\": \"Alice\",\n" +
                "\t\t\"arcana\": \"Death\",\n" +
                "\t\t\"level\": 79,\n" +
                "\t\t\"stats\": [43, 59, 40, 57, 45],\n" +
                "\t\t\"elems\": [\"-\", \"-\", \"-\", \"-\", \"-\", \"-\", \"rs\", \"rs\", \"wk\", \"rp\"],\n" +
                "\t\t\"skills\": {\n" +
                "\t\t\t\"Dekunda\": 0,\n" +
                "\t\t\t\"Die For Me!\": 81,\n" +
                "\t\t\t\"Mamudoon\": 0,\n" +
                "\t\t\t\"Megidolaon\": 82,\n" +
                "\t\t\t\"Concentrate\": 83,\n" +
                "\t\t\t\"Mudo Boost\": 0,\n" +
                "\t\t\t\"Survival Trick\": 84\n" +
                "\t\t},\n" +
                "\t\t\"special\": true,\n" +
                "\t\t\"max\": true\n" +
                "\t}]";
    }
}
