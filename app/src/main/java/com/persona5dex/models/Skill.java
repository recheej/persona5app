package com.persona5dex.models;

/**
 * Created by Rechee on 8/8/2017.
 */

public class Skill {
    private String name;
    private int level;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
