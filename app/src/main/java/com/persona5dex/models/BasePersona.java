package com.persona5dex.models;

import java.util.HashMap;

/**
 * Created by Rechee on 7/1/2017.
 */

public class BasePersona {
    public String name;
    public int level;
    HashMap<String, Integer> skills;
    public boolean special;
    public boolean max;
    public boolean dlc;
    public boolean rare;
    public String arcanaName;
    public int id;
}
