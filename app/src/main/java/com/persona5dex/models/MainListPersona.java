package com.persona5dex.models;


/**
 * Created by Rechee on 11/18/2017.
 */

public class MainListPersona {
    public int id;
    public String name;
    public String arcanaName;
    public Enumerations.Arcana arcana;
    public int level;
    public boolean rare;
    public boolean dlc;
}
