package com.persona5dex.models;

/**
 * Created by Rechee on 7/12/2017.
 */

public class RawPersonaEdge {
    public int start;
    public int end;

    public int pairPersona;
}
