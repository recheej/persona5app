package com.persona5dex.dagger.fusionService;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Scope;

/**
 * Created by Rechee on 6/18/2017.
 */

@Scope
@Retention(RetentionPolicy.CLASS)
public @interface FusionServiceScope {
}
